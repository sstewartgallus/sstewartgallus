#!/usr/bin/env python
#
# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import sys

BASE_DIR = os.path.dirname(__file__)

def manage():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ssg.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

def start_uwsgi():
    parser = argparse.ArgumentParser(
        description = 'Starts up the web server')
    parser.add_argument(
        'URI',
        type = str,
        help = 'The uri to run the server under')

    parser.add_argument(
        'PORT',
        type = str,
        help = 'The port to run the server under')

    arguments = parser.parse_args()
    parser = None

    uri = arguments.URI
    port = arguments.PORT
    arguments = None

    os.environ['PYTHONHASHSEED'] = 'random'

    os.execvp('uwsgi',
              ['uwsgi',
               '--ini', os.path.join(BASE_DIR, 'etc', 'uwsgi.ini'),
               '--static-map', '/static=static',
               '--http', uri + ':' + port,
               '--wsgi-file', os.path.join(BASE_DIR, 'wsgi.py')])

if __name__ == "__main__":
    manage()
