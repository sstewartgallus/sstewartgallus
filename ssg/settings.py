# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Django settings for ssg project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
import os
import random

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(__file__)

def tolist(x):
    if not x:
        return x
    return x.split(',')

# SECURITY WARNING: don't run with debug turned on in production!
try:
    DEBUG = os.environ['SSG_DEBUG']
    if 'True' == DEBUG:
        DEBUG = True
    elif 'False' == DEBUG:
        DEBUG = False
    else:
        raise Error('Bad Value')
except KeyError:
    DEBUG = False

# SECURITY WARNING: don't run with secure cookies turned off in production!
try:
    SSG_SECURE_COOKIES = os.environ['SSG_SECURE_COOKIES']
    if 'True' == SSG_SECURE_COOKIES:
        SSG_SECURE_COOKIES = True
    elif 'False' == SSG_SECURE_COOKIES:
        SSG_SECURE_COOKIES = False
    else:
        raise Error('Bad Value')
except KeyError:
    SSG_SECURE_COOKIES = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY_FILE = os.environ.get('SSG_SECRET_KEY', 'secretkey')

SSG_DB = os.environ.get('SSG_DB', 'ssg.sqlite3')
STATIC_ROOT = os.environ.get('SSG_STATIC_ROOT', 'static')
SSG_LOG = os.environ.get('SSG_LOG', 'ssg.log')
ALLOWED_HOSTS = tolist(os.environ.get('SSG_ALLOWED_HOSTS', []))
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'root@localhost')
ADMINS = (('Steven Stewart-Gallus', 'sstewartgallus00@mylangara.bc.ca'),)


try:
    with open(SECRET_KEY_FILE) as f:
        SECRET_KEY = f.read().strip()
except:
    with open(SECRET_KEY_FILE, mode='w') as f:
        SECRET_KEY = ''.join([random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(50)])
        f.write(SECRET_KEY)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
             [os.path.join(BASE_DIR, 'templates')]
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,

            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CSRF_COOKIE_SECURE = SSG_SECURE_COOKIES
SESSION_COOKIE_SECURE = SSG_SECURE_COOKIES

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ssg.front'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'ssg.urls'

WSGI_APPLICATION = 'ssg.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': SSG_DB
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': SSG_LOG
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True
        },
        'ssg': {
            'handlers': ['file'],
            'level': 'DEBUG'
        },
    },
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-ca'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

CONN_MAX_AGE = 20
