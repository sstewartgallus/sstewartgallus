# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class CustomUserData(models.Model):
    """User with app settings."""
    user = models.OneToOneField(User)

    twitter = models.CharField(max_length=50, blank = True)
    facebook = models.CharField(max_length=50, blank = True)

    def __str__(self):
        return self.user.__str__()

    class Meta:
        verbose_name_plural = "custom user data"

class UserInterest(models.Model):
    """User interests."""
    user = models.ForeignKey(User)

    interest = models.CharField(max_length=50, blank = True)

    def __str__(self):
        return self.user.__str__()

class UserPosition(models.Model):
    """User positions."""
    user = models.OneToOneField(User)

    last_updated = models.DateTimeField(auto_now = True)

    x = models.DecimalField(max_digits = 19, decimal_places = 4)
    y = models.DecimalField(max_digits = 19, decimal_places = 4)
    z = models.DecimalField(max_digits = 19, decimal_places = 4)

    def __str__(self):
        return self.user.__str__()
