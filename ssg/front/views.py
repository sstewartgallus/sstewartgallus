# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import django.contrib.auth

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.middleware.csrf import get_token
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views import generic
from django.views.decorators.http import require_http_methods

from django import forms

from ssg.front.forms import LoginForm, PasswordSettingsForm, SignupForm, SettingsForm
from ssg.front.models import CustomUserData, UserInterest

_our_content_type = "text/html"

@require_http_methods(['GET'])
def index(request):
    return _render_index(request)


@require_http_methods(['GET'])
def cosmology(request):
    return render(request, 'front/cosmology.html',
                  content_type = _our_content_type)

@require_http_methods(['GET'])
def lore(request):
    return render(request, 'front/lore.html',
                  content_type = _our_content_type)

@require_http_methods(['GET'])
def dirty_tricks(request):
    return render(request, 'front/dirty-tricks.html',
                  content_type = _our_content_type)

@require_http_methods(['GET'])
def mutation(request):
    return render(request, 'front/mutation.html',
                  content_type = _our_content_type)

def _render_index(request, **kwargs):
    user = request.user

    csrf_token_value = get_token(request)

    data = None
    interests = None
    if user.is_authenticated():
        data = CustomUserData.objects.get(user = user)
        interests_list = UserInterest.objects.filter(user = user).order_by('user').values_list('interest', flat = True)

    login_form = kwargs.get('login_form')
    if None == login_form:
        login_form = LoginForm()

    signup_form = kwargs.get('signup_form')
    if None == signup_form:
        signup_form = SignupForm()

    password_settings_form = kwargs.get('password_settings_form')
    if None == password_settings_form:
        password_settings_form = PasswordSettingsForm()

    settings_form = kwargs.get('settings_form')
    if None == settings_form and data:
        twitter = data.twitter
        facebook = data.facebook
        interests = ' '.join(interests_list)

        settings = {}
        if twitter:
            settings['twitter'] = twitter
        if facebook:
            settings['facebook'] = facebook

        if interests:
            settings['interests'] = interests

        settings_form = SettingsForm(settings)

    new_kwargs = {}

    new_kwargs['login_form'] = login_form
    new_kwargs['signup_form'] = signup_form

    if settings_form:
        new_kwargs['settings_form'] = settings_form

    new_kwargs['csrf_token_value'] = csrf_token_value

    return render(request, 'front/index.html', new_kwargs,
                  content_type = _our_content_type)
