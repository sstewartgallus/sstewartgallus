# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""My simple website
"""

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='SSG',

    version='0.0.1',

    description='SSG\'s website',
    long_description=long_description,

    url='https://gitlab.com/sstewartgallus/sstewartgallus',

    author='Steven Stewart-Gallus',
    author_email='sstewartgallus00@mylangara.bc.ca',

    license='Apache 2',

    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Myself',
        'Topic :: Other',

        'License :: OSI Approved :: Apache 2',

        'Programming Language :: Python :: 3.4'
    ],

    keywords='',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    install_requires=['django', 'pyproj', 'uwsgi'],

    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    package_data={
        'ssg': ['front/static/front/css/**', 'front/static/front/images/**', 'front/templates/front/**',
                'static/**', 'templates/admin/**',
                'etc/**'],
    },

    entry_points={
        'console_scripts': [
            'ssg_manage=ssg:manage',
            'ssg=ssg:start_uwsgi',
        ],
    },
)
